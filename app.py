import sys
import json
import traceback

from flask import request, Flask, render_template, redirect, url_for
from wiki import determine_status, NotFound

app = Flask(__name__)

@app.route('/')
def index():
	return render_template('index.html')

def not_found(topic):
	return render_template('notfound.html', name=topic)

@app.errorhandler(500)
def show_error():
	return render_template('error.html')

def make_anchor(name, url):
	return f'<a href="{url}">{name}</a>'

specials = set([
	'alex_wainwright',
	'alexander_wainwright',
	'alex wainwright',
	'alexander wainwright',
	'peter turner',
	'simon mcnaughton',
	])

special_status = {
		'alex': 'alex will never die',
		'peter': 'Peter has been alive for thousands of years',
		'simon': 'Simon is eternal'
		}

@app.route('/<topic>')
def show_article(topic):
	try:
		if topic.lower() in specials:
			name = topic.split()[0]
			return render_template('article_noimage.html',
					topic=name,
					name=name,
					status=special_status[name],
					)
		data = determine_status(topic)
	except NotFound:
		return not_found(topic)
	except Exception as e:
		traceback.print_exc()
		print(f'Error {str(e())}', file=sys.stderr)
		return show_error()

	if not data['is_person']:
		return not_found(topic)

	if data['dead']:
		status = 'No, {} is dead.'.format(
				make_anchor(
					name=data['title'],
					url=data['page_url']
					))
	else:
		status = 'Yes, {} is alive.'.format(
				make_anchor(
					name=data['title'],
					url=data['page_url']
					))

	if data['img_url'] in ('', None):
		template = 'article_noimage.html'
	else:
		template = 'article.html'

	death_date = None
	if data['date_of_death']:
		death_date = data['date_of_death'].strftime('%-d %B %Y')

	return render_template(template,
			topic=data['title'],
			name=data['title'],
			status=status,
			img_url=data['img_url'],
			attribution=data['attribution'],
			date_of_death=death_date,
			)

@app.route('/submit')
def submit():
	return redirect(url_for('show_article', topic=request.args.get('topic')))
	input_text = request.args.get('topic')
	return show_article(input_text)

if __name__ == '__main__':
	app.run(debug=True)

# psuedocode:
'''
1. is the person noteworthy? ask gpt
	-> if no end with apology
2. what is the person's wikipedia page? find out somehow.  maybe get gpt to give
   the full name and use some kind of wikipedia api
3. curl the wiki page and extract the first paragraph
4. pass the text to gpt and ask it if the person is alive
'''
