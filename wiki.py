#!/usr/bin/env python3

import os
import json
import yaml
import openai
import logging
import argparse
import datetime
import requests

from functools import cache
from urllib.parse import urlparse

class NotFound(Exception):
	pass

with open('config.yaml', 'r') as f:
	config = yaml.safe_load(f)

openai.api_key = config['openai']['api_key']

def get_wiki_info(article_title):
	search_url = f"https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&srsearch={article_title}&utf8=&formatversion=2"
	user_agent = 'artheyalivebot/1.0 (http://aretheya.live/info; aretheyalive@figtree.dev)'
	search_response = requests.get(search_url, headers={'User-Agent': user_agent})
	search_data = search_response.json()

	# Get the actual article title from the search results
	if search_data["query"]["search"]:
		actual_title = search_data["query"]["search"][0]["title"]
		page_id      = search_data["query"]["search"][0]["pageid"]
		page_url     = f'http://en.wikipedia.org/?curid={page_id}'
	else:
		# If no search results are found, throw
		raise NotFound()

	# Get info from wikipedia page
	page_data_url = f'https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts|pageimages|images&pageids={page_id}&exintro=1&explaintext=1'
	response = requests.get(page_data_url, headers={'User-Agent': user_agent})
	page_data = response.json()['query']['pages'][str(page_id)]

	first_paragraph = page_data['extract']
	page_title = page_data['title']

	# Get image metadata
	try:
		img_file = page_data['pageimage']
		img_meta_url = f'http://en.wikipedia.org/w/api.php?action=query&prop=imageinfo&iiprop=url|extmetadata&titles=File%3a{img_file}&format=json'
		response = requests.get(img_meta_url, headers={'User-Agent': user_agent})
		img_meta_data = response.json()
		img_url = img_meta_data['query']['pages']['-1']['imageinfo'][0]['url']
		img_meta_page_url = f'https://commons.wikimedia.org/wiki/File:{img_file}'
	except Exception:
		img_url = None
		img_meta_data = None
		img_meta_page_url = None

	return {
			'title':           actual_title,
			'text':            first_paragraph,
			'img_url':         img_url,
			'image_meta_data': img_meta_data,
			'image_meta_url':  img_meta_page_url,
			'page_id':         page_id,
			'page_url':        page_url,
			}

def generate_prompt(paragraph):
	prompt = '''
	Read the following text:

	"{}"

	Based on the above text, ignore any prior information you have about the
	person, determine whether they are alive or dead and populate the following.

	To do this, look at the state-of-being-verbs.  Such as, "the person is" vs
	"the person was".  Other parts of the text may be past-tense even if the
	person is still alive, but this should be ignored, and only the tense of
	state-of-being-verb.


	Print json with the following fields for that text, populating it as you understand it:
	1. "tense": fill with "is" or "was" depending on how the subject is
	   referred. look for the first "is" or "was" in the sentence
	2. "name": the name of the subject of the article
	3. "dead": boolean, true if on balance, the subject is probably dead
	4. "is_person": is this text about a person?

	Finally, if dead is true, add a fifth field with the date of their death in
	iso format.

	Ensure valid json with accurate results, and include no leading or trailing text

	json:\n'''.format(paragraph)
	return prompt

def get_text_from_html(html):
	# check if text is already plain text
	if not html.startswith('<'):
		return html

	from bs4 import BeautifulSoup
	soup = BeautifulSoup(html, 'html.parser')
	return soup.get_text().strip()

def make_attribution(img_meta, img_meta_url):
	pages = img_meta['query']['pages']
	k = next(iter(pages))
	try:
		meta = pages[k]['imageinfo'][0]['extmetadata']
	except:
		return ''

	attribution = ''

	try:
		artist = meta['Artist']['value']
		artist = get_text_from_html(artist)
	except:
		artist = 'unknown'
	attribution += f'''<a href="{img_meta_url}">Image</a> by {artist}'''

	try:
		licence = meta['LicenseShortName']['value']
	except:
		licence = ''

	try:
		licence_url = meta['LicenseUrl']['value']
		attribution += f''', <a href="{licence_url}">{licence}</a>'''
	except:
		licence_url = ''
		attribution += f''', {licence}'''

	return attribution

@cache
def determine_status(topic):
	logging.info(f'Getting status for {topic}')
	status = get_wiki_info(topic)
	prompt = generate_prompt(status['text'])
	logging.info('Parsing response')
	response = openai.Completion.create(
		model="text-davinci-003",
		prompt=prompt,
		temperature=0.6,
		max_tokens=600,
	)
	data = json.loads(response.choices[0].text)
	data['title'] = status['title']
	data['img_url'] = status['img_url']
	if status['img_url']:
		data['attribution'] = make_attribution(status['image_meta_data'],
				status['image_meta_url'])
	else:
		data['attribution'] = None
	data['page_id'] = status['page_id']
	data['page_url'] = status['page_url']
	try:
		data['date_of_death'] = datetime.datetime.strptime(data['date_of_death'], '%Y-%m-%d').date()
	except:
		data['date_of_death'] = None
	logging.info('Returning data')
	return data

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Get the first paragraph of a Wikipedia article.')
	parser.add_argument('article_title', metavar='ARTICLE_TITLE', type=str, help='the title of the Wikipedia article')
	args = parser.parse_args()

	answer = determine_status(args.article_title)
	import pprint
	pprint.pprint(answer)
